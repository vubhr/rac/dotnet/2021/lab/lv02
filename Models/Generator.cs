namespace Lottery.Models;

public class Generator {
  public static SortedSet<int> LottoNumbers(int count, int maxNumber) {
    Random random = new Random();
    SortedSet<int> numbers = new SortedSet<int>();
    while (numbers.Count < count) {
      numbers.Add(random.Next(1, maxNumber + 1));
    }
    return numbers;
  }
}