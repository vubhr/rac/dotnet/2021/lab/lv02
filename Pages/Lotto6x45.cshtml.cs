using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Lottery.Models;

namespace Lottery.Pages {
  public class Lotto6x45Model : PageModel {
    public Lotto6x45Model() {
      Numbers = Generator.LottoNumbers(6, 45);
    }

    public void OnGet() {
    }

    private static SortedSet<int> LottoNumbers(int count, int maxNumber) {
      Random random = new Random();
      SortedSet<int> numbers = new SortedSet<int>();
      while (numbers.Count < count) {
        numbers.Add(random.Next(1, maxNumber + 1));
      }
      return numbers;
    }

    public SortedSet<int> Numbers { get; set; }
  }
}
