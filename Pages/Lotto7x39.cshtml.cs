using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Lottery.Models;

namespace Lottery.Pages {
  public class Lotto7x39Model : PageModel {
    public Lotto7x39Model() {
      Numbers = Generator.LottoNumbers(7, 39);
    }

    public void OnGet() {
    }

    private static SortedSet<int> LottoNumbers(int count, int maxNumber) {
      Random random = new Random();
      SortedSet<int> numbers = new SortedSet<int>();
      while (numbers.Count < count) {
        numbers.Add(random.Next(1, maxNumber + 1));
      }
      return numbers;
    }

    public SortedSet<int> Numbers { get; set; }
  }
}
